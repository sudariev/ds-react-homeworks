const arrModalWindow = [
    {
        id: 0,
        header: "Do you want to delete this file?",
        text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
        closeButton: true,
        actions: (
            <>
                <button className="modal__button-ok">Ok</button>
                <button className="modal__button-cancel">Cancel</button>
            </>
        ),
    },
    {
        id: 1,
        header: "Proceed to delete file?",
        text: "After deletion the file this operation can't be undone. Continue?",
        closeButton: true,
        actions: (
            <>
                <button className="modal__button-continue">Continue</button>
                <button className="modal__button-abort">Abort</button>
            </>
        ),
    },
];
export default arrModalWindow;
