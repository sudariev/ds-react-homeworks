import React from "react";
import "./Modal.scss";

class Modal extends React.Component {
    render() {
        const { id, header, text, closeButton, actions, closeModal } =
            this.props;
        return (
            <div className="modal" onClick={() => this.props.closeModal()}>
                <div
                    className="modal__content"
                    onClick={(e) => e.stopPropagation()}
                    key={id}
                >
                    <div className="modal__header">{header}</div>
                    {closeButton && (
                        <button
                            onClick={() => closeModal()}
                            className="modal__close-button"
                        >
                            +
                        </button>
                    )}
                    <div className="modal__text">{text}</div>
                    <div className="modal__button-confirm">{actions}</div>
                </div>
            </div>
        );
    }
}

export default Modal;
