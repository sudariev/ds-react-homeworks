import React from "react";
import "./Button.scss";

class Button extends React.Component {
    render() {
        const { text, style, openModal, dataModalId } = this.props;
        return (
            <button
                data-modal-id={dataModalId}
                className="button__open-modal"
                style={style}
                onClick={() => openModal(dataModalId)}
            >
                {text}
            </button>
        );
    }
}

export default Button;
