import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "../../index.css";
import arrModalWindow from "../../arrModalWindow.js"

class App extends React.Component {
    constructor(){
        super();
        this.state = {
            isOpen: false,
            arrModalWindow: {},
        };
    }

    openModal = (id) => {
        this.setState({
            isOpen: true,
            arrModalWindow: {
                ...arrModalWindow[id],
            },
        });
    };
    
    closeModal = () => {
        this.setState({
            isOpen: false,
        });
    };

    render() {
        const { id, header, text, closeButton, actions } = this.state.arrModalWindow;
        return (
            <>
                <div className="button">
                    <Button
                        style={{ backgroundColor: "green" }}
                        text="Open first modal"
                        openModal={this.openModal}
                        dataModalId={0}
                    />
                    <Button
                        style={{ backgroundColor: "royalblue" }}
                        text="Open second modal"
                        openModal={this.openModal}
                        dataModalId={1}
                    />
                </div>
                {this.state.isOpen && (
                    <Modal
                        id={id}
                        header={header}
                        text={text}
                        closeButton={closeButton}
                        actions={actions}
                        closeModal={this.closeModal}
                    />
                )}
            </>
        );
    }
};

export default App;
